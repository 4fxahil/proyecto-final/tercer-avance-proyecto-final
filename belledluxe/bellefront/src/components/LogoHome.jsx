import React from 'react';
import logobdl from '../assets/images/logobdl.png';

const navBackgroundColor = "#11131F";
const logoStyle = {
    display: 'block',
    margin: 'auto',
    marginBottom : "4%",
  };


export default function LogoHome() {
  return (
    <img
      src={logobdl}
      alt="nature image"
      style={{...logoStyle, backgroundColor: navBackgroundColor}}
    />
  );
}
