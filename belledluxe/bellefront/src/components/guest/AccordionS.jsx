import React from "react";
import {
  Accordion,
  AccordionHeader,
  AccordionBody,
} from "@material-tailwind/react";

import cuadroLavado from "../../assets/images/cuadroLavado.png";
import cuadroSecado from "../../assets/images/cuadroSecado.png";
import cuadroPlanchado from "../../assets/images/cuadroPlanchado.png";
 
export default function AccordionS() {
  const [open, setOpen] = React.useState(1);
 
  const handleOpen = (value) => setOpen(open === value ? 0 : value);
 
  return (


    <>
      <p className="text-white p-12 text-1xl text-justify">

Es importante tener los cuidados necesarios de la ropa es la mejor manera de mantenerla en buen estado
      y amplia su durabilidad. Nos hemos convertido en consumidores activos, responsables, conscientes e impulsores
      para contribuir a su propio traje que al mismo tiempo ayuda al entorno ambiental. 
      Tenemos para nuestro consumidores, algunos consejos para mantener la esencia de la vestimenta.


</p>
<br />

      <Accordion open={open === 1} className="mb-2 border border-white px-4">
        <AccordionHeader
          onClick={() => handleOpen(1)}
          className={`border-b-0 transition-colors ${
            open === 1 ? "text-white hover:!text-white" : ""
          }`}
          style={{ fontFamily: "Times New Roman, serif"}}
        >
         Planchado y cuidado
        </AccordionHeader>
        <AccordionBody className="pt-0 text-base text-white text-1xl"   style={{ fontFamily: "Times New Roman, serif"}}>
        <br/>
            I. Antes de planchar, clasifica las prendas segun el tipo de
            tela y el nivel de calor que requieren. <br/>
            <br/>

            II. Utiliza una tela humeda para proteger las telas delicadas
            como el encaje y la lana <br/>
            <br/>

            III. Rocia las prendas de algodon y poliester con agua o vapor
            antes de plancharlas
            <br /> <br/>
            <img src={cuadroPlanchado} alt="Formas de cuerpo" style={{ maxWidth: '40%', height: 'auto' }} />
            <br /> <br/>

           <hr/>


        </AccordionBody>
      </Accordion>
      <Accordion open={open === 2} className="mb-2  border border-white px-4">
        <AccordionHeader
          onClick={() => handleOpen(2)}
          className={`border-b-0 transition-colors ${
            open === 2 ? "text-white hover:!text-white" : ""
          }`}
          style={{ fontFamily: "Times New Roman, serif"}}
        >
         Lavado y secado
        </AccordionHeader>
        <AccordionBody className="pt-0 text-base text-white text-1xl"   style={{ fontFamily: "Times New Roman, serif"}}>
                    I. Revise los simbolos de lavado. Indica si la prenda se puede
            lavar a mano o a maquina a que temperatura, y si se puede usar
            secadora o plancha.

<br />
<br />
II. Para lavar a mano, usa agua tibia, y poco jabon,
            preferiblemente especial para prendas delicadas
<br />
<br />
<img src={cuadroLavado} alt="Formas de cuerpo" style={{ maxWidth: '40%', height: 'auto' }} />
<br /> <br/>
III. Para secar la ropa, lo mejor es hacerlo al aire libre,
            lejos del sol directo. No usar secadora su la etiqueta lo
            prohibe. Si se utiliza secadora, escoja una temperatura baja y
            un programa corto.
            <br /> <br/>
            <img src={cuadroSecado} alt="Formas de cuerpo" style={{ maxWidth: '40%', height: 'auto' }} />

<br />
<br />
<hr />
        </AccordionBody>
      </Accordion>
      <Accordion open={open === 3} className="border border-white px-4">
        <AccordionHeader
          onClick={() => handleOpen(3)}
          className={`border-b-0 transition-colors ${
            open === 3 ? "text-white hover:!text-white" : ""
          }`}
          style={{ fontFamily: "Times New Roman, serif"}}
        >
          Calidad y costura
        </AccordionHeader>
        <AccordionBody className="pt-0 text-base text-white text-1xl"   style={{ fontFamily: "Times New Roman, serif"}}>
        BELLE D' LUXE usa tejidos naturales y nobles como la seda, el
            algodon, la lana o el cachemir, que aportan suavidad, comodidad
            y durabilidad a las prendas. Tambien emplean materiales
            sinteticos o mexclados, pero siempre de alta calidad y con
            propiedades especiales, como la elasticidad, la transpirabilidad
            o la resistencia. Los materiales tambien influyen en el aspecto
            y el color de las prendas, asi como en su caida y su movimiento.
<br />
<br />
        </AccordionBody>
      </Accordion>
    </>
  );
}