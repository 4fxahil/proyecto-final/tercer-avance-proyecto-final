import React, { useState } from 'react';
import AccordionS from './AccordionS';
import AccordionA from './AccordionA';
import "../../css/Tabs/tabs.css"

const TabsS = () => {
  const [activeTab, setActiveTab] = useState('asesoramiento');

  const changeTab = (tab) => {
    setActiveTab(tab);
  };

  return (
    <div className="flex flex-col items-center">
      <div className="flex space-x-4">
        <button
          className={`tab-btn ${activeTab === 'asesoramiento' ? 'active' : ''}`}
          onClick={() => changeTab('asesoramiento')}
        >
          Asesoramiento de Vestimenta
        </button>
        <button
          className={`tab-btn ${activeTab === 'cuidado' ? 'active' : ''}`}
          onClick={() => changeTab('cuidado')}
        >
          Cuidado y Lavado
        </button>
      </div>
      <div className="underline"></div>
      <div className="content">
        {activeTab === 'asesoramiento' && 
          <div>
          <AccordionA/>
          {/* Agrega más acordeones según sea necesario */}
        </div>
        }
        {activeTab === 'cuidado' && <div>
        <AccordionS/>
            </div>}
        
      </div>
    </div>
  );
};

export default TabsS;
