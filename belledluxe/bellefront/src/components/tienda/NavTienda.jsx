
import React, { useState, useEffect } from "react";
import { Menu } from "@headlessui/react";
import {
  ShoppingCartIcon,
  UserCircleIcon,
  ChevronLeftIcon,
} from "@heroicons/react/outline";
import { useAuthStore } from "../../store/auth";
import { useNavigate } from 'react-router-dom';


const NavTienda = () => {

  const navigate = useNavigate();

  const isAdmin = useAuthStore((state) => state.admin)
  
  const [isOpen, setIsOpen] = useState(false);
  const navBackgroundColor = "#11131F";

  const [superior, setsuperior] = useState('login');
  const [inferior, setinferior] = useState('register');
  const [link1, setlink1]= useState('login')
  const [link2, setlink2]= useState('singup')


  const isLoggedIn = useAuthStore((state) => state.isLoggedIn)  
  useEffect(()=>{
    if(isLoggedIn()){
      setsuperior('Perfil')
      setinferior('Salir')
      setlink1('/perfil')
      setlink2('/salir')
    }else{
      setsuperior('acceder')
      setinferior('registrar')
      setlink1('/login')
      setlink2('/register')
    }
  },[])

  return (
    <nav className="p-6" style={{ backgroundColor: navBackgroundColor }}>
      <div>

<button onClick={() => navigate(-1)}>
<ChevronLeftIcon className="h-6 w-6 text-white mr-4" />
</button>


</div>
      <div className="flex justify-end">
        {/* Iconos */}
        <div className="flex ">
          {/* Icono de perfil */}
          <Menu as="div">
            <Menu.Button>
              <UserCircleIcon className="h-6 w-6 text-white" />
            </Menu.Button>
            <Menu.Items className="absolute right-0 px-8 py-4 rounded-md shadow-lg bg-white ring-1 ring-black ring-opacity-5 focus:outline-none">
              <div>
                <Menu.Item>
                  {({ active }) => (
                    <a
                      href={link1}
                      className={`${active ? "bg-gray-100" : ""} block py-2 `}
                    >
                      {superior}
                      <div>
                          {isAdmin ? (
                              <h1>Admin!</h1>
                              
                             
                          ) : (
                            <></>

                          )}
                      </div>
                    </a>
                    
                  )}
                </Menu.Item>
                <Menu.Item>
                  {({ active }) => (
                    <a
                      href={link2}
                      className={`${active ? "bg-gray-100" : ""} block`}
                    >
                      {inferior}
                    </a>
                  )}
                </Menu.Item>
                {/* Otros elementos de menú */}
              </div>
            </Menu.Items>
          </Menu>
          {/* Icono de carrito */}
          <div className="ml-4">
            <a href="/carrito">
            <ShoppingCartIcon  className="h-6 w-6 text-white" />
            </a>
          </div>
        </div>
      </div>
    </nav>
  );
};

export default NavTienda;
