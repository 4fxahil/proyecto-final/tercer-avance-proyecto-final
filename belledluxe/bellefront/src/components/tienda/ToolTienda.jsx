import React from 'react';
import logobdl from '../../assets/images/logobdl.png';

const ToolTienda = () => {
  return (
    <nav className='border-b'>
      <a href="#">
        <img src={logobdl} alt="Logo" style={{ width: '950px', height: 'auto' }} />
      </a>
     
    </nav>
  );
};

export default ToolTienda;
