import { useEffect, useState } from "react";
import axios from "../../utils/axios";
import Cookies from 'js-cookie';
import { useNavigate } from "react-router-dom";

export default function FunPer() {
    const navigate = useNavigate()
    const [userData, setUserData] = useState(null);
    const [error, setError] = useState(null);
    const [loading, setLoading] = useState(true);
  
    useEffect(() => {
      async function fetchData() {
        try {
          const accessToken = Cookies.get('access_token');
          const response = await axios.get('perfil/', {
            headers: { Authorization: `Bearer ${accessToken}` } 
          });
          setUserData(response.data); 
        } catch (error) {
          console.error('Error fetching user data:', error);
          setError('Error fetching user data. Please try again later.');
        } finally {
          setLoading(false);
        }
      }
  
      fetchData();
    }, []);
  
    if (loading) {
      return <div style={{ 'color':'#ffffff'}}>Cargando...</div>;
    }

    if (error) {
      return <div style={{ 'color':'#ffffff'}}>{error}</div>;
    }

    const handleClick = () => {
        navigate('../fomulario/direccion');
      };    
    const AdminClick = () => {
        navigate('../adm');
      };
  
    const { username, profile } = userData;
    if (!profile) {
      return(
        <div className="bg-blue-998 antialiased">
          <div className="container mx-auto my-60">
            <div>
    
              <div className="bg-white relative shadow rounded-lg w-5/6 md:w-5/6 lg:w-4/6 xl:w-3/6 mx-auto">
                <div className="flex justify-center">
                  <img
                    src="https://static.vecteezy.com/system/resources/previews/007/167/661/non_2x/user-blue-icon-isolated-on-white-background-free-vector.jpg"
                    alt=""
                    className="rounded-full mx-auto absolute -top-20 w-32 h-32 shadow-md border-4 border-white transition duration-200 transform hover:scale-110"
                  />
                </div>
    
                <div className="mt-16">
                  <h1 className="font-bold text-center text-3xl text-gray-900"> Admin </h1>
                  <p className="text-center text-sm text-gray-400 font-medium"> Usted es admin </p>
                  <p>
                    <span>
    
                    </span>
                  </p>
                  <div className="my-5 px-6">
                    <a onClick={AdminClick} className="text-gray-200 block rounded-lg text-center font-medium leading-6 px-6 py-3 bg-gray-800 hover:bg-black hover:text-white"><span className="font-bold"> Acceder a panel administrativo </span></a>
                  </div>
                  <br />
  
                </div>
              </div>
    
            </div>
          </div>
        </div>
    )
  
      }else{
        const { name_first, last_name_pat, last_name_mat, phone, cp, streat, number_home } = profile;
        if(streat ===null || streat === ''){
          return(
            <div className="bg-blue-998 antialiased">
              <div className="container mx-auto my-60">
                <div>
        
                  <div className="bg-white relative shadow rounded-lg w-5/6 md:w-5/6 lg:w-4/6 xl:w-3/6 mx-auto">
                    <div className="flex justify-center">
                      <img
                        src="https://static.vecteezy.com/system/resources/previews/007/167/661/non_2x/user-blue-icon-isolated-on-white-background-free-vector.jpg"
                        alt=""
                        className="rounded-full mx-auto absolute -top-20 w-32 h-32 shadow-md border-4 border-white transition duration-200 transform hover:scale-110"
                      />
                    </div>
        
                    <div className="mt-16">
                      <h1 className="font-bold text-center text-3xl text-gray-900"> {name_first} </h1>
                      <p className="text-center text-sm text-gray-400 font-medium"> {last_name_pat} {last_name_mat} </p>
                      <p>
                        <span>
        
                        </span>
                      </p>
                      <div className="my-5 px-6">
                        <a onClick={handleClick} className="text-gray-200 block rounded-lg text-center font-medium leading-6 px-6 py-3 bg-gray-800 hover:bg-black hover:text-white"><span className="font-bold"> Actualizar dirección </span></a>
                      </div>
        
        
                      <div className="w-full">
                        <h3 className="font-medium text-gray-900 text-left px-6">Información : </h3>
                        
                        <div className="mt-5 w-full flex flex-col items-center overflow-hidden text-sm">
        
        
                          <a className="w-full border-t border-gray-100 text-gray-600 py-4 pl-6 pr-3 w-full block hover:bg-gray-100 transition duration-150">
                            <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTXE1xN1qcUoQkyxyKGDJYFlH4IboDiZ7uTg1oERVr9Jw&s" alt="" className="rounded-full h-6 shadow-md inline-block mr-2" />
                            Numero Telefonico : <span className="font-bold"> </span>
                            <span className="text-gray-500 text-xs"> {phone} </span>
                          </a>   
        
                        </div>
                      </div>
                    </div>
                  </div>
        
                </div>
              </div>
            </div>
        )}else{ 
          return(
            <>
            
            <div className="bg-blue-998 antialiased">
            <div className="container mx-auto my-60">
              <div>
      
                <div className="bg-white relative shadow rounded-lg w-5/6 md:w-5/6 lg:w-4/6 xl:w-3/6 mx-auto">
                  <div className="flex justify-center">
                    <img
                      src="https://static.vecteezy.com/system/resources/previews/007/167/661/non_2x/user-blue-icon-isolated-on-white-background-free-vector.jpg"
                      alt=""
                      className="rounded-full mx-auto absolute -top-20 w-32 h-32 shadow-md border-4 border-white transition duration-200 transform hover:scale-110"
                    />
                  </div>
      
                  <div className="mt-16">
                    <h1 className="font-bold text-center text-3xl text-gray-900"> {name_first} </h1>
                    <p className="text-center text-sm text-gray-400 font-medium"> {last_name_pat} {last_name_mat} </p>
                    <p>
                      <span>
      
                      </span>
                    </p>
                    <div className="my-5 px-6">
                      <a onClick={handleClick} className="text-gray-200 block rounded-lg text-center font-medium leading-6 px-6 py-3 bg-gray-800 hover:bg-black hover:text-white"><span className="font-bold"> Actualizar dirección </span></a>
                    </div>
      
      
                    <div className="w-full">
                      <h3 className="font-medium text-gray-900 text-left px-6">Información : </h3>
                      
                      <div className="mt-5 w-full flex flex-col items-center overflow-hidden text-sm">
      
      
                        <a className="w-full border-t border-gray-100 text-gray-600 py-4 pl-6 pr-3 w-full block hover:bg-gray-100 transition duration-150">
                          <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTXE1xN1qcUoQkyxyKGDJYFlH4IboDiZ7uTg1oERVr9Jw&s" alt="" className="rounded-full h-6 shadow-md inline-block mr-2" />
                          Numero Telefonico : <span className="font-bold"> </span>
                          <span className="text-gray-500 text-xs"> {phone} </span>
                        </a>
      
                        <a className="w-full border-t border-gray-100 text-gray-600 py-4 pl-6 pr-3 w-full block hover:bg-gray-100 transition duration-150">
                          <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTXE1xN1qcUoQkyxyKGDJYFlH4IboDiZ7uTg1oERVr9Jw&s" alt="" className="rounded-full h-6 shadow-md inline-block mr-2" />
                          Código postal :
                          <span className="text-gray-500 text-xs"> {cp} </span>
                        </a>
      
                        <a className="w-full border-t border-gray-100 text-gray-600 py-4 pl-6 pr-3 w-full block hover:bg-gray-100 transition duration-150 overflow-hidden">
                          <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTXE1xN1qcUoQkyxyKGDJYFlH4IboDiZ7uTg1oERVr9Jw&s" alt="" className="rounded-full h-6 shadow-md inline-block mr-2" />
                          Dirección :
                          <span className="text-gray-500 text-xs"> {streat} {number_home} </span>
                        </a>
      
      
                      </div>
                    </div>
                  </div>
                </div>
      
              </div>
            </div>
          </div></>
          );
        }
      }
      
  };
  
  
  