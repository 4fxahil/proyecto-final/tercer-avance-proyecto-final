import React from "react";
import ImgBlog from "../../assets/images/ImgBlog.png";
import "../../css/blog/bloggers.css";

export default function Bloggers() {
  return (
    <div className="relative w-full">
      <div className="w-full h-full">
        <img
          src={ImgBlog}
          alt="Descripción de la imagen"
          className="w-full h-full object-cover border"
        />
      </div>
      <div className="absolute bottom-0 right-0 p-4">
        <a href="https://nuestro-blog-belledeluxe.blogspot.com/2023/11/tendencias-de-moda.html" target="_blank">
          <button className="border-b border-white px-4 py-2 bg-transparent text-white">Descúbrelo aquí</button>
        </a>    
      </div>
      <div className="absolute top-0 left-0 p-4 text-white text-container">
        <h2 className="text-4xl font-bold p-4">Personaliza tu estilo para estas temporadas</h2>
        <p className="text-lg">Tenemos para ti las tendencias de moda para la época de Invierno-Otoño. Mantente al día con las últimas novedades en el mundo de la moda.</p>
      </div>
    </div>
  );
}
