import React from 'react';

const Boxcontact
 = ({ title, description }) => {
  return (
    <div className="box-about" style={{marginBottom : "8%", marginTop : "6%", marginLeft : "4%"}}>
    <h2 className="font-bold p-4">Tambien nos puedes encontrar en nuestras redes sociales</h2>
    <p>En BelleD'Luxe nos interesa que tengas una buena relacion con nuestro sitio web, por lo que si
        tienes alguna duda nos lo hagas saber.
    </p>
  </div>
  );
};

export default Boxcontact; 

