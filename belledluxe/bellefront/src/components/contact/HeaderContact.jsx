export default function HeaderContact() {
    return (
      <p
        className="ml-md-4 mt-md-1"
        style={{
          fontWeight: "bold",
          fontSize: "150%",
          textAlign: "justify",
          color: "white",
          marginLeft:"5%",
          marginTop: "0%",  
          marginBottom: "0%",
        }}
      >
        En BelleDeLuxe nos aseguramos de que tengas las mejores experiencias realizando tus compras 
        por lo que, estariamos encantados de saber si tu estancia con nostros fue agradable o si te 
        gustarìa dejarnos saber alguna sugerencia. 
      </p>
      
    );
  }

  