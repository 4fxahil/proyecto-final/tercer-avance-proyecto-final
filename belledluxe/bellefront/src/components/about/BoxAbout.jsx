import React from 'react';

const BoxAbout
 = ({ title, description }) => {
  return (
    <div className="box-about" style={{marginBottom : "8%", marginTop : "6%"}}>
    <h2 className="font-bold p-4">{title}</h2>
    <p>{description}</p>
  </div>
  );
};

export default BoxAbout;