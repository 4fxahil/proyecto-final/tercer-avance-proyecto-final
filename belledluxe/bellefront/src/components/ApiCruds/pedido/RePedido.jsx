import React, { useEffect, useState } from "react";
import { getAllPedidos } from "../../../api/Pedido_api";
import VistPedido from "./VistPedido";

export default function RePedido() {
    const [Pedido, setPedido] = useState([]);

    useEffect(() => {
        async function loadPedidos(){
            const resp = await getAllPedidos();
            setPedido(resp.data);
        }
        loadPedidos();
    }, []);

    return(
        <div className="bg-gray-900 text-white min-h-screen p-8">
            <h1 className="text-3xl font-bold mb-6">Pedidos</h1>
            <div className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 gap-6">
                {Pedido.map(Pedido => (
                    <VistPedido key={Pedido.id} Pedido = {Pedido} />
                ))}
            </div>
        </div>
    );
}
