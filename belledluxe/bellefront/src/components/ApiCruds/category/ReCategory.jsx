import React, { useEffect, useState } from "react";
import { Link } from 'react-router-dom';
import { getAllCategories } from "../../../api/Category_api";
import VistCategory from '../category/VistCategory';

export default function ReCategory() {
    const [Category, setCategories] = useState([]);

    useEffect(() => {
        async function loadCategories(){
            const resp = await getAllCategories();
            setCategories(resp.data);
        }
        loadCategories();
    }, []);

    return (
        <div className="bg-gray-900 text-white min-h-screen p-8">
            <div className="flex justify-between items-center mb-6">
                <h1 className="text-3xl font-bold">Categorias</h1>
                <Link to="../CategoryForm" className="bg-blue-500 text-white py-2 px-4 rounded hover:bg-blue-600 focus:outline-none focus:bg-blue-600">Crear nueva categoria</Link>
            </div>
            <div className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 gap-6">
                {Category.map(Category => (
                    <VistCategory key={Category.id} Category={Category} />
                ))}
            </div>
        </div>
    );
}
