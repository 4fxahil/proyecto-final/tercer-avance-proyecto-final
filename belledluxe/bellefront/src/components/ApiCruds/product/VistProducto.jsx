import React from 'react';
import { useNavigate } from 'react-router-dom';

export default function VistProduct({ Product }) {
    const navigate = useNavigate();

    const handleClick = () => {
        navigate('../ReProducto/' + Product.id);
    };

    return (
        <div
            style={{
                border: '1px solid #eee',
                borderRadius: '8px',
                padding: '20px',
                marginBottom: '20px',
                cursor: 'pointer',
                transition: 'all 0.3s ease',
                boxShadow: '0 2px 4px rgba(0, 0, 0, 0.1)',
                backgroundColor: '#fff',
                color: '#333',
            }}
            onClick={handleClick}
        >
            <img
                src={Product.imagen1}
                alt="Product"
                style={{ maxWidth: '100%', marginBottom: '10px' }}
            />
            <h1 style={{ margin: '5px 0', fontSize: '22px', color: '#555' }}>ID: {Product.id}</h1>
            <h2 style={{ margin: '5px 0', fontSize: '20px', color: '#222' }}>{Product.name}</h2>
            <p style={{ margin: '5px 0', fontSize: '18px', color: '#444' }}>{Product.gender}</p>
            <p style={{ margin: '5px 0', fontSize: '18px', color: '#444' }}>{Product.description}</p>
            <p style={{ margin: '5px 0', fontSize: '18px', color: '#444' }}>Talla: {Product.size}</p>
            <p style={{ margin: '5px 0', fontSize: '18px', color: '#444' }}>$ {Product.price}</p>
            <p style={{ margin: '5px 0', fontSize: '18px', color: '#444' }}>Estado: {Product.status}</p>
            <p style={{ margin: '5px 0', fontSize: '18px', color: '#444' }}>Stock: {Product.stock}</p>
            <p style={{ margin: '5px 0', fontSize: '18px', color: '#444' }}>Campaña: {Product.campaign}</p>
            <p style={{ margin: '5px 0', fontSize: '18px', color: '#444' }}>Categoría: {Product.category}</p>
            <img
                src={Product.imagenqr}
                alt="Product"
                style={{ maxWidth: '100%', marginBottom: '10px' }}
            />
        </div>
    );
}
