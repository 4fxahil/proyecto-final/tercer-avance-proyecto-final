import React from "react";
import AdminSlidebar from "../components/AdminSlidebar";
import AdminWar from "../components/AdminWar";

export default function AdminPage(){
    return(
        <div className="min-h-screen flex">
            <AdminSlidebar />
            <div className="flex-1 p-6">
            <AdminWar/>
            </div>
      </div>
    );
}