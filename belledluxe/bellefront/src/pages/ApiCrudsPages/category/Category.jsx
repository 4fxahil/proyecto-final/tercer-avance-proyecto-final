import React from "react";
import ReCategory from "../../../components/ApiCruds/category/ReCategory";
import AdminSlidebar from "../../../components/AdminSlidebar";

export default function Category() {
    return (
        <div className="min-h-screen flex">
        <AdminSlidebar />
        <div className="flex-1 p-6">
        <ReCategory/>
        </div>
      </div>
    );
}