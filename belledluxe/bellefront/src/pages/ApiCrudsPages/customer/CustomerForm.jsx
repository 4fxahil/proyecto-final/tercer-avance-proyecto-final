import React, { useEffect, useState } from "react";
import { useForm } from 'react-hook-form';
import { getCustomer, UpdateCustomer, DeleteCustomer } from "../../../api/Customer_api";
import { getAllUsers } from "../../../api/Users";
import { useNavigate, useParams } from 'react-router-dom';
import { toast } from 'react-hot-toast';
import NavApi from "../../../components/ApiCruds/NavApi";
import FooterApi from "../../../components/ApiCruds/FooterApi";

export default function CustomerForm() {
    const [users, setUsers] = useState([]);
    const { register, handleSubmit, formState: { errors }, setValue } = useForm();
    const navigate = useNavigate();
    const params = useParams();
    const handleClick = () => {
        navigate('../ReCustomer/');
    };

    useEffect(() => {
        async function loadData() {
            const usersResponse = await getAllUsers();
            setUsers(usersResponse.data);
            if (params.id) {
                const { data } = await getCustomer(params.id);
                setValue('user', data.user);
                setValue('name_first', data.name_first);
                setValue('last_name_pat', data.last_name_pat);
                setValue('last_name_mat', data.last_name_mat);
                setValue('phone', data.phone);
                setValue('cp', data.cp);
                setValue('streat', data.streat);
                setValue('number_home', data.number_home);
            }
        }
        loadData();
    }, [params.id, setValue]);

    const onSubmit = handleSubmit(async data => {
        if (params.id) {
            await UpdateCustomer(params.id, data);
            toast.success('Cliente actualizado con éxito');
        } else {
            toast.error('Usuario no encontrado, intentelo de nuevo');
        }
        navigate('../ReCustomer');
    });

    return (
        <div> 

            <NavApi/>

        <div className="container mx-auto mt-10 text-white">
            <form onSubmit={onSubmit} className="max-w-md mx-auto">
                <br />

                <label htmlFor="user" className="block mb-2">Usuario:</label>
                <select id="user" {...register("user", { required: true })} className="block w-full px-4 py-2 mb-4 border border-gray-600 rounded focus:outline-none bg-gray-800 text-white">
                    {users.map(user => (
                        <option key={user.id} value={user.id}>{user.username}</option>
                    ))}
                </select>
                {errors.user && <span className="text-red-500">Este campo es requerido</span>}

                <label htmlFor="name_first" className="block mb-2">Nombre:</label>
                <input type="text" id="name_first" {...register("name_first", { required: true })} className="block w-full px-4 py-2 mb-4 border border-gray-600 rounded focus:outline-none bg-gray-800 text-white focus:border-blue-500 focus:ring-blue-500"  placeholder="Nombre" maxLength={30} />
                {errors.name_first && <span className="text-red-500">Este campo es requerido</span>}

                <label htmlFor="last_name_pat" className="block mb-2">Primer apellido:</label>
                <input type="text" id="last_name_pat" {...register("last_name_pat", { required: true })} className="block w-full px-4 py-2 mb-4 border border-gray-600 rounded focus:outline-none bg-gray-800 text-white focus:border-blue-500 focus:ring-blue-500"  placeholder="Primer apellido" maxLength={12}/>
                {errors.last_name_pat && <span className="text-red-500">Este campo es requerido</span>}

                <label htmlFor="last_name_mat" className="block mb-2">Segundo apellido:</label>
                <input type="text" id="last_name_mat" {...register("last_name_mat")} className="block w-full px-4 py-2 mb-4 border border-gray-600 rounded focus:outline-none bg-gray-800 text-white focus:border-blue-500 focus:ring-blue-500"  placeholder="Segundo apellido" maxLength={12}/>

                <label htmlFor="phone" className="block mb-2">Número de celular:</label>
                <input type="text" id="phone" {...register("phone", { required: true, pattern: /^[0-9]*$/ })} className="block w-full px-4 py-2 mb-4 border border-gray-600 rounded focus:outline-none bg-gray-800 text-white focus:border-blue-500 focus:ring-blue-500"  placeholder="Número de celular" maxLength={10}/>
                {errors.phone && errors.phone.type === "required" && <span className="text-red-500">Este campo es requerido</span>}
                {errors.phone && errors.phone.type === "pattern" && <span className="text-red-500">Ingrese un número válido</span>}

                <label htmlFor="cp" className="block mb-2">Código Postal:</label>
                <input type="text" id="cp" {...register("cp", { required: true, pattern: /^[0-9]*$/ })} className="block w-full px-4 py-2 mb-4 border border-gray-600 rounded focus:outline-none bg-gray-800 text-white focus:border-blue-500 focus:ring-blue-500"  placeholder="Código Postal" maxLength={5}/>
                {errors.cp && errors.cp.type === "required" && <span className="text-red-500">Este campo es requerido</span>}
                {errors.cp && errors.cp.type === "pattern" && <span className="text-red-500">Ingrese un código postal válido</span>}

                <label htmlFor="streat" className="block mb-2">Calle:</label>
                <input type="text" id="streat" {...register("streat", { required: true })} className="block w-full px-4 py-2 mb-4 border border-gray-600 rounded focus:outline-none bg-gray-800 text-white focus:border-blue-500 focus:ring-blue-500"  placeholder="Calle" maxLength={40} />
                {errors.streat && <span className="text-red-500">Este campo es requerido</span>}

                <label htmlFor="number_home" className="block mb-2">Número de casa:</label>
                <input type="text" id="number_home" {...register("number_home", { required: true, pattern: /^[0-9]*$/ })} className="block w-full px-4 py-2 mb-4 border border-gray-600 rounded focus:outline-none bg-gray-800 text-white focus:border-blue-500 focus:ring-blue-500"  placeholder="Número de casa" maxLength={6} />
                {errors.number_home && errors.number_home.type === "required" && <span className="text-red-500">Este campo es requerido</span>}
                {errors.number_home && errors.number_home.type === "pattern" && <span className="text-red-500">Ingrese un número válido</span>}




                <button type="submit" className="block w-full px-4 py-2 bg-blue-500 rounded hover:bg-blue-600 focus:outline-none focus:bg-blue-600">Guardar</button>

                {params.id &&
                <button onClick={async () => {
                    const accepted = window.confirm("¿Estás seguro de que deseas eliminar este cliente definitivamente?");
                    if (accepted) {
                        await DeleteCustomer(params.id);
                        navigate('../ReCustomer');
                        toast.success('Perfil eliminado');                
                    }
                }} className="block w-full px-4 py-2 mt-4 bg-red-500 rounded hover:bg-red-600 focus:outline-none focus:bg-red-600">Eliminar</button>}
            
            <br />
                <hr />
                <br />
                <button onClick={handleClick} className="block w-full px-4 py-2 bg-gray-500 rounded hover:bg-gray-600 focus:outline-none focus:bg-gray-600">Cancelar</button>


            </form> 


        </div>

<br />
        <FooterApi/>

        </div>


    
    );
}
