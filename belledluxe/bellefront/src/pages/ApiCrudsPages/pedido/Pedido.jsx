import React from "react";
import RePedido from "../../../components/ApiCruds/pedido/RePedido";
import AdminSlidebar from "../../../components/AdminSlidebar";

export default function Pedido(){
    return(
        <div className="min-h-screen flex">
        <AdminSlidebar />
        <div className="flex-1 p-6">
        <RePedido/>
        </div>
      </div>
    );
}