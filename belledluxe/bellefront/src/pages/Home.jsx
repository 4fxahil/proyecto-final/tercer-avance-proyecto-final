import NavHome from "../components/NavHome";
import LogoHome from "../components/LogoHome";
import ToolHome from "../components/ToolHome";
import BannerHome from "../components/BannerHome";
import FooterHome from "../components/FooterHome";
import CardHome from "../components/CardHome";


import cardHomeOne from "../assets/images/cardHomeOne.png";
import cardHomeTwo from "../assets/images/cardHomeTwo.png";
import cardHomeAll from "../assets/images/cardHomeAll.png";


const containerStyle = {
  backgroundColor: "#11131F",
  border: "1px solid white",
  fontFamily: "Times New Roman, Baskerville, serif",
};

export default function Home() {
  return (
 
    <div>

      <NavHome />
      <LogoHome />
      <ToolHome />
      <BannerHome />
      <CardHome></CardHome>
      <br/>
      <br/>
      <br/>
      <div className="container mx-auto pl-4">
  <div className="grid grid-cols-1 sm:grid-cols-2 md:grid-cols-3 gap-4 justify-center ">
    <CardHome
      title={<a href="tienda/hombre/">Hombre</a>}
      description="Conjuntos formales, Pantalones, Camisas, Trajes, Accesorios"
      imageUrl={cardHomeTwo}
    />
    <CardHome
      title={<a href="tienda/mujer">Mujer</a>}
      description="Vestidos largos o cortos formales y elegantes, Conjuntos de ropa, Accesorios."
      imageUrl={cardHomeOne}
    />
    <CardHome
      title={<a href={`/tienda`}>Todo</a>}
      description="Vestimenta de hombre y mujer para eventos formales y elegantes."
      imageUrl={cardHomeAll}
    />
  </div>
</div>




 
    <br/>
      <br/>
      <br/>

      <FooterHome />

    </div>

  );
}
