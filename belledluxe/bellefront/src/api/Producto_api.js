import axios from 'axios'


const ProductosApi = axios.create({
    baseURL:'http://localhost:8000/api/cruds/productos/',
  
})


export const uploadProductImage = (id, imageFile) => {
    const formData = new FormData();
    formData.append('imagen1', imageFile);
  
    return ProductosApi.post(`/`, formData, {
      headers: {
        'Content-Type': 'multipart/form-data' // add this line
      }
    });
  };

export const getAllProducts  = () => ProductosApi.get('/');
export const getProduct = (id) => ProductosApi.get('/' + id + '/');
export const createProduct = async (product) => {
    const formData = new FormData();
    formData.append('name', product.name);
    formData.append('description', product.description);
    formData.append('price', product.price);
    formData.append('stock', product.stock);
    formData.append('category', product.category);
    formData.append('campaign', product.campaign);
    formData.append('gender', product.gender);
    formData.append('size', product.size);
    formData.append('status', product.status);
    formData.append('slug', product.slug);
    formData.append('lasteditor', product.lasteditor);
    formData.append('imagen1', product.imagen1[0]);
    
    if (product.imagenqr && product.imagenqr.length > 0) {
        formData.append('imagenqr', product.imagenqr[0]);
    }

    const response = await ProductosApi.post('/', formData, {
      headers: {
        'Content-Type': 'multipart/form-data'
      }
    });
  
    return response.data;
  };

export const DeleteProduct = (id) => ProductosApi.delete('/' + id); 
export const UpdateProduct = async (id, product) => {
    const formData = new FormData();
    formData.append('name', product.name);
    formData.append('description', product.description);
    formData.append('price', product.price);
    formData.append('stock', product.stock);
    formData.append('category', product.category);
    formData.append('campaign', product.campaign);
    formData.append('gender', product.gender);
    formData.append('size', product.size);
    formData.append('status', product.status);
    formData.append('slug', product.slug);
    formData.append('lasteditor', product.lasteditor);
    formData.append('imagen1', product.imagen1[0]);

    // Verifica si existe un valor en product.imagenqr antes de adjuntarlo
    if (product.imagenqr && product.imagenqr.length > 0) {
        formData.append('imagenqr', product.imagenqr[0]);
    }

    const response = await ProductosApi.put(`/${id}/`, formData, {
        headers: {
            'Content-Type': 'multipart/form-data'
        }
    });

    return response.data;
};
