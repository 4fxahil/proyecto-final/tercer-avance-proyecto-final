import { useAuthStore } from '../store/auth';
import { Navigate } from 'react-router-dom';

const AdminRuta = ({ children }) =>{
    const isAdmin = useAuthStore((state) => state.admin)
    return isAdmin ? <>{children}</> : <Navigate to="/" />;
       
}
export default AdminRuta;